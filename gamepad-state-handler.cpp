#include <string>
#include <set>
#include "gamepad-state-handler.h"
#include <regex>
#include "game-functions/range-of-keys-game-function.h"
#include "game-functions/two-keys-game-function.h"
#include "game-functions/single-key-game-function.h"
#include "game-functions/mouse-moving-function.h"
#include <X11/keysym.h>
#include <algorithm>
#include <iostream>
#include "defaults.h"
#include "game-functions/mouse-key-function.h"
#include "split-string.h"

GamepadStateHandler::GamepadStateHandler(Display* display)
{
    this->functionNames = {
        {MappingType::GamepadBoolState, {"WEAPON_CLASS_PREVIOUS", "WEAPON_CLASS_NEXT", "WEAPON_CLASS_SAME", "TIMER_REDUCE", "TIMER_INCREASE", "SPACE", "UP", "LEFT", "RIGHT", "DOWN", "ENTER", "SPACE", "SHIFT", "BACKSPACE", "GRENADE_BOUNCE_TOGGLE", "CAMERA_TOGGLE", "RIGHT_MOUSE_KEY", "LEFT_MOUSE_KEY", "TAB", "ESC", "DELETE", "INSERT"}},
        {MappingType::GamepadNumericState, {"MOUSE_HORIZONTAL", "MOUSE_VERTICAL"}},
        {MappingType::GamepadNumericStateNumericSetting, {"MOUSE_HORIZONTAL_MAX_SPEED", "MOUSE_VERTICAL_MAX_SPEED"}},
        {MappingType::GamepadNumericStateCurveSetting, {"MOUSE_HORIZONTAL_CURVE", "MOUSE_VERTICAL_CURVE"}}
    };
    this->pWeaponsFunction = nullptr;
    this->pTimerFunction = nullptr;
    this->pCameraToggle = nullptr;
    this->pGrenadeBounceToggle = nullptr;
    this->_display = display;
    this->singleKeyFunctionKeySyms = {
        {"UP", {XK_Up}},
        {"LEFT", {XK_Left}},
        {"RIGHT", {XK_Right}},
        {"DOWN", {XK_Down}},
        {"ENTER", {XK_Return}},
        {"SPACE", {XK_space}},
        {"SHIFT", {XK_Shift_R}},
        {"BACKSPACE", {XK_BackSpace}},
        {"TAB", {XK_Tab}},
        {"ESC", {XK_Escape}},
        {"DELETE", {XK_Delete}},
        {"INSERT", {XK_Insert}}
    };
    this->curveSettings = {
        {"MOUSE_HORIZONTAL_CURVE", DEFAULT_MOUSE_MOVE_CURVE},
        {"MOUSE_VERTICAL_CURVE", DEFAULT_MOUSE_MOVE_CURVE},
    };
    this->mouseSpeedSettings = {
        {"MOUSE_HORIZONTAL_MAX_SPEED", DEFAULT_MOUSE_MOVE_MAX_SPEED},
        {"MOUSE_VERTICAL_MAX_SPEED", DEFAULT_MOUSE_MOVE_MAX_SPEED},
    };
}
std::string GamepadStateHandler::GetError(MappingType mappingType, std::string strWithMistake)
{
    switch(mappingType) {
        case MappingType::GamepadBoolState:
            return strWithMistake + " is not a string describing pressing";
        break;
        case MappingType::UnknownMappingName:
            return "Unknown key: " + strWithMistake;
        break;
        case MappingType::GamepadNumericState:
            return strWithMistake + " is not a string describing pressing or axis";
        break;
        case MappingType::GamepadNumericStateNumericSetting:
            return strWithMistake + " is not a positive number";
        break;
        case MappingType::GamepadNumericStateCurveSetting:
            return strWithMistake + " is not a curve name";
        break;
    }
    return "";
}

MappingType GamepadStateHandler::getMappingNameType(std::string mappingKey) {
    for (auto elem = this->functionNames.begin(); elem != this->functionNames.end(); ++elem) {
        if (elem->second.find(mappingKey) != elem->second.end()) {
            return elem->first;
        }
    }
    return MappingType::UnknownMappingName;
}
std::string GamepadStateHandler::AddMapping(std::string functionKey, std::string mappingString)
{
    const auto type = this->getMappingNameType(functionKey);
    if (type == MappingType::UnknownMappingName) {
        return "Unknown key: " + functionKey;
    }
    if (type == MappingType::GamepadBoolState || type == MappingType::GamepadNumericState) {
        std::vector<std::string> parts = SplitString(mappingString, ";");
        const std::regex pressingMappingRegex(R"(^(Trigger|Button)\d+$|^Axis\d+[+-]$)");
        const std::regex numericValueMappingRegex(R"(^Axis\d+$)");
        bool isMappingStringPartOk;
        for (const auto& part : parts) {
            isMappingStringPartOk = type == MappingType::GamepadBoolState
                ? std::regex_search(part, pressingMappingRegex)
                : std::regex_search(part, pressingMappingRegex) || std::regex_search(part, numericValueMappingRegex);
            if (!isMappingStringPartOk) {
                return this->GetError(type, part);
            }
        }
        this->mappings[functionKey] = parts;
    } else {
        const std::regex settingValueRegex = type == MappingType::GamepadNumericStateNumericSetting
            ? std::regex(R"(^[\d\.]+$)")
            : std::regex(R"(^(Quadratic|Cubic|QuadraticExtreme|SquareRoot)$)");
        if (!std::regex_search(mappingString, settingValueRegex)) {
            return this->GetError(type, mappingString);
        }
        if (MappingType::GamepadNumericStateNumericSetting == type) {
            this->mouseSpeedSettings[functionKey] = std::stod(mappingString);
        } else {
            this->curveSettings[functionKey] = mappingString;
        }
    }
    return "";
}
std::string GamepadStateHandler::FinishMapping() {
    for (auto elem = this->mappings.begin(); elem != this->mappings.end(); ++elem) {
        if (elem->first.find("WEAPON_CLASS") != std::string::npos) {
            if (this->pWeaponsFunction != nullptr) {
                continue;
            }
            if (this->mappings["WEAPON_CLASS_PREVIOUS"].size() == 0 || this->mappings["WEAPON_CLASS_NEXT"].size() == 0) {
                return "When specifying weapons switching, both WEAPON_CLASS_PREVIOUS and WEAPON_CLASS_NEXT must be specified";
            }

            std::vector<std::vector<KeySym>> weaponClassButtons = {{XK_grave}};
            std::set<std::string> sameClassTrigger = this->toSet(this->mappings["WEAPON_CLASS_SAME"]);
            for (unsigned i = 0; i < 12; i++) {
                weaponClassButtons.push_back(std::vector<KeySym>{XK_F1+i});
            }
            this->pWeaponsFunction = new RangeOfKeysGameFunction(
                this->_display,
                weaponClassButtons,
                this->toSet(this->mappings["WEAPON_CLASS_PREVIOUS"]),
                this->toSet(this->mappings["WEAPON_CLASS_NEXT"]),
                sameClassTrigger.size() ? &sameClassTrigger : nullptr
            );
        } else if (elem->first.find("TIMER") != std::string::npos) {
            if (this->pTimerFunction != nullptr) {
                continue;
            }
            if (this->mappings["TIMER_REDUCE"].size() == 0 || this->mappings["TIMER_INCREASE"].size() == 0) {
                return "When specifying timer switching, both TIMER_REDUCE and TIMER_INCREASE must be specified";
            }

            std::vector<std::vector<KeySym>> timerButtons;
            for (unsigned i = 0; i < 5; i++) {
                timerButtons.push_back(std::vector<KeySym>{XK_1+i});
            }
            this->pTimerFunction = new RangeOfKeysGameFunction(
                this->_display,
                timerButtons,
                this->toSet(this->mappings["TIMER_REDUCE"]),
                this->toSet(this->mappings["TIMER_INCREASE"]),
                nullptr
            );
        } else if (elem->first == "CAMERA_TOGGLE") {
            this->pCameraToggle = new TwoKeysGameFunction(
                this->_display,
                std::unordered_set<KeySym>{XK_Home},
                std::unordered_set<KeySym>{XK_Control_R, XK_Home},
                this->toSet(this->mappings["CAMERA_TOGGLE"])
            );
        } else if (elem->first == "GRENADE_BOUNCE_TOGGLE") {
            this->pGrenadeBounceToggle = new TwoKeysGameFunction(
                this->_display,
                std::unordered_set<KeySym>{XK_equal},
                std::unordered_set<KeySym>{XK_minus},
                this->toSet(this->mappings["GRENADE_BOUNCE_TOGGLE"])
            );
        } else if (elem->first == "MOUSE_HORIZONTAL" || elem->first == "MOUSE_VERTICAL") {
            std::smatch axisMatch;
            std::regex axisRegex(R"(^Axis(\d+)$)");
            std::set<std::string> trigger;
            uint8_t* pAxis = nullptr;
            for (const auto& mappedItem : this->mappings[elem->first]) {
                if (std::regex_search(mappedItem, axisMatch, axisRegex)) {
                    pAxis = new uint8_t;
                    *pAxis = std::stoi(axisMatch.str(1));;
                } else {
                    trigger.insert(mappedItem);
                }
            }
            if (pAxis == nullptr) {
                return "for key "+elem->first+" at least one axis must be specified";
            }
            this->mouseMovingFunctions.push_back(
                MouseMovingFunction(
                    this->_display,
                    elem->first == "MOUSE_HORIZONTAL" ? MouseMovingDirection::Horizontal : MouseMovingDirection::Vertical,
                    this->mouseSpeedSettings[elem->first + "_MAX_SPEED"],
                    trigger,
                    *pAxis,
                    this->curveSettings[elem->first + "_CURVE"]
                )
            );
        } else if (elem->first == "LEFT_MOUSE_KEY" || elem->first =="RIGHT_MOUSE_KEY") {
            this->mouseKeyFunctions.push_back(
                MouseKeyGameFunction(
                    this->_display,
                    elem->first == "LEFT_MOUSE_KEY" ? 1 : 3,
                    this->toSet(this->mappings[elem->first])
                )
            );
        } else {
            auto functionKeySyms = this->singleKeyFunctionKeySyms[elem->first];
            if (functionKeySyms.size() != 0) {
                this->singleKeyFunctions.push_back(
                    SingleKeyGameFunction(
                        this->_display,
                        functionKeySyms,
                        this->toSet(this->mappings[elem->first])
                    )
                );
            }
        }
    }
    return "";
}
template<class T>
std::set<T> GamepadStateHandler::toSet(std::vector<T> vec)
{
    std::set<T> set;
    for (const auto& elem : vec) {
        set.insert(elem);
    }
    return set;
}
void GamepadStateHandler::OnGamepadStateChanged(std::set<std::string> gamepadBoolState, std::map<uint8_t, int16_t> gamepadNumericState)
{
    for (auto& singleKeyFunction : this->singleKeyFunctions) {
        singleKeyFunction.SetState(this->IsTriggerActive(gamepadBoolState, singleKeyFunction.GetTrigger(), false));
    }
    if (this->pWeaponsFunction != nullptr) {
        if (this->pWeaponsFunction->HasTriggerSame()) {
            this->pWeaponsFunction->SetState(this->IsTriggerActive(gamepadBoolState, this->pWeaponsFunction->GetTriggerSame(), true), RangeOfKeysIndexAction::Stay);
        }
        this->pWeaponsFunction->SetState(this->IsTriggerActive(gamepadBoolState, this->pWeaponsFunction->GetTriggerPrevious(), true), RangeOfKeysIndexAction::Decrease);
        this->pWeaponsFunction->SetState(this->IsTriggerActive(gamepadBoolState, this->pWeaponsFunction->GetTriggerNext(), true), RangeOfKeysIndexAction::Increase);
    }
    if (this->pTimerFunction != nullptr) {
        this->pTimerFunction->SetState(this->IsTriggerActive(gamepadBoolState, this->pTimerFunction->GetTriggerPrevious(), true), RangeOfKeysIndexAction::Decrease);
        this->pTimerFunction->SetState(this->IsTriggerActive(gamepadBoolState, this->pTimerFunction->GetTriggerNext(), true), RangeOfKeysIndexAction::Increase);
    }
    if (this->pCameraToggle != nullptr) {
        this->pCameraToggle->SetState(this->IsTriggerActive(gamepadBoolState, this->pCameraToggle->GetTrigger(), true));
    }
    if (this->pGrenadeBounceToggle != nullptr) {
        this->pGrenadeBounceToggle->SetState(this->IsTriggerActive(gamepadBoolState, this->pGrenadeBounceToggle->GetTrigger(), true));
    }
    for (auto& mouseMovingFunction : this->mouseMovingFunctions) {
        if (gamepadNumericState[mouseMovingFunction.GetDependency()] == 0 || this->IsTriggerActive(gamepadBoolState, mouseMovingFunction.GetTrigger(), false)) {
            mouseMovingFunction.SetState(gamepadNumericState[mouseMovingFunction.GetDependency()]);
        }
    }
    for (auto& mouseKeyFunction : this->mouseKeyFunctions) {
        mouseKeyFunction.SetState(this->IsTriggerActive(gamepadBoolState, mouseKeyFunction.GetTrigger(), true));
    }
}
bool GamepadStateHandler::IsTriggerActive(std::set<std::string> gamepadState, std::set<std::string> trigger, bool fullEqualityOnly)
{
    for (const std::string& gamepadButtonOrAxisState : trigger) {
        if (std::find(gamepadState.begin(), gamepadState.end(), gamepadButtonOrAxisState) == gamepadState.end()) {
            return false;
        }
    }
    return fullEqualityOnly ? trigger.size() == gamepadState.size() : true;
}

