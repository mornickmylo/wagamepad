#include <iostream>
#include <SDL.h>
#include "wa-gamepad.h"
#include <unistd.h>
#include <limits.h>

int main(int argc, char **argv) {
    if (!XInitThreads()) {
        std::cout << "X server multithreading could not initialize!" << std::endl;
        return 1;
    }
    if(SDL_Init( SDL_INIT_JOYSTICK ) < 0)
    {
        std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
        return 1;
    }
    std::cout << "SDL initialized!" << std::endl;
    int gamepadsAmount = SDL_NumJoysticks();
    std::cout << "Gamepads amount: " << gamepadsAmount << std::endl;
    if (gamepadsAmount > 0) {
        WAGamepad mainProgram;
        std::string error = mainProgram.start(0, "wagamepad.config");
        if (error != "") {
            std::cout << error << std::endl;
            return 1;
        }
    }
    SDL_Quit();
    return 0;
}
