#include "wa-gamepad.h"
#include <set>
#include <SDL.h>
#include "defaults.h"
#include <iostream>
#include "gamepad-state-handler.h"
#include <fstream>
#include "split-string.h"

WAGamepad::WAGamepad()
{
    this->gamepadEvents = std::set<Uint32>{SDL_EventType::SDL_JOYAXISMOTION, SDL_EventType::SDL_JOYBUTTONDOWN, SDL_EventType::SDL_JOYBUTTONUP, SDL_EventType::SDL_JOYHATMOTION};
    this->joystickBoolStateDeadZone = DEFAULT_JOYSTICK_BOOL_STATE_DEADZONE;
    this->joystickNumericStateDeadZone = DEFAULT_JOYSTICK_NUMERIC_STATE_DEADZONE;
    this->triggers = nullptr;
}
std::string WAGamepad::start(int joystickIndex, std::string configPath)
{
    this->gGameController = SDL_JoystickOpen( joystickIndex );
    if (gGameController == NULL)
    {
        return "Warning: Unable to open game controller! SDL Error: " + std::string(SDL_GetError());
    }
    this->display = XOpenDisplay(NULL);
    this->handler = new GamepadStateHandler(this->display);
    std::string error = "";
    std::map<std::string, std::string> config;
    if (this->ReadConfig(configPath, &config) != 0) {
        return "error reading config file \"" + configPath + "\"";
    }
    for (const auto& configKeyValuePair : config) {
        error += this->handler->AddMapping(configKeyValuePair.first, configKeyValuePair.second);
        if (error != "") {
            return error;
        }
    }
    error = this->handler->FinishMapping();
    if (error != "") {
        return error;
    }
    this->mainLoop();
    return "";
}
void WAGamepad::getTriggers()
{
    const int axisesAmout = SDL_JoystickNumAxes(this->gGameController);
    std::cout << "Gamepad triggers: ";
    this->triggers = new std::set<int>;
    Sint16 axisState;
    for (int axis = 0; axis < axisesAmout; axis++) {
        axisState = SDL_JoystickGetAxis(this->gGameController, axis);
        //std::cout << "Axis " << axis << " state: " << axisState << std::endl;
        if (axisState < -32000) {
            this->triggers->insert(axis);
            std::cout << axis << ",";
        }
    }
    std::cout << std::endl;
}
void WAGamepad::handleGemapadEvent()
{
    if(this->e.type == SDL_EventType::SDL_JOYHATMOTION)
    {
        std::cout << "Hat value: " << unsigned(this->e.jhat.value) << std::endl;
    }
    else if (this->e.type == SDL_EventType::SDL_JOYAXISMOTION)
    {
        const std::string axisStr = std::to_string(this->e.jaxis.axis);
        if (triggers->find(this->e.jaxis.axis) != triggers->end()) {
            if (this->e.jaxis.value > 0) {
                this->gamepadBoolState.insert("Trigger"+axisStr);
            } else {
                this->gamepadBoolState.erase("Trigger"+axisStr);
            }
        } else {
            if (this->e.jaxis.value > this->joystickBoolStateDeadZone) {
                this->gamepadBoolState.insert("Axis"+axisStr+"+");
                this->gamepadBoolState.erase("Axis"+axisStr+"-");
            }  else if (this->e.jaxis.value < -this->joystickBoolStateDeadZone){
                this->gamepadBoolState.erase("Axis"+axisStr+"+");
                this->gamepadBoolState.insert("Axis"+axisStr+"-");
            } else {
                this->gamepadBoolState.erase("Axis"+axisStr+"+");
                this->gamepadBoolState.erase("Axis"+axisStr+"-");
            }
            if (this->e.jaxis.value > this->joystickNumericStateDeadZone || this->e.jaxis.value < -this->joystickNumericStateDeadZone) {
                this->gamepadNumericState[this->e.jaxis.axis] = e.jaxis.value;
            } else {
                this->gamepadNumericState[this->e.jaxis.axis] = 0;
            }
        }
    }
    else if (this->e.type == SDL_EventType::SDL_JOYBUTTONDOWN || this->e.type == SDL_EventType::SDL_JOYBUTTONUP)
    {
        const bool isPressed = this->e.type != SDL_EventType::SDL_JOYBUTTONUP;
        const std::string btnStr = "Button" + std::to_string(this->e.jbutton.button);
        if (isPressed) {
            this->gamepadBoolState.insert(btnStr);
        } else {
            this->gamepadBoolState.erase(btnStr);
        }
    }
    this->logStringSetIfChanged(this->gamepadBoolState);
    handler->OnGamepadStateChanged(this->gamepadBoolState, this->gamepadNumericState);
}

void WAGamepad::mainLoop()
{
    std::cout << "pTriggers=" << this->triggers << std::endl;
    while (1) {
        if (SDL_PollEvent(&this->e) != 0)
        {
            //User requests quit
            if (e.type == SDL_EventType::SDL_QUIT)
            {
                SDL_JoystickClose(this->gGameController);
                break;
            }
            else if (this->gamepadEvents.find(e.type) != this->gamepadEvents.end())
            {
                if (this->triggers == nullptr) {
                    this->getTriggers();
                }
                handleGemapadEvent();
            }
        }
    }
}
void WAGamepad::logStringSetIfChanged(std::set<std::string> set)
{
    static std::string setStr;
    std::string newSetStr = "";
    for (const std::string& str : set) {
        newSetStr += str + ";";
    }
    if (newSetStr != setStr) {
        setStr = newSetStr;
        std::cout << (setStr == "" ? "---" : setStr) << std::endl;
    }
}
uint8_t WAGamepad::ReadConfig(std::string configPath, std::map<std::string, std::string>* pConfig)
{
    std::fstream configFile(configPath);
    std::string configLine;
    if (!configFile.is_open()) {
        return -1;
    }
    while (getline(configFile, configLine)) {
        auto configKeyValuePair = SplitString(configLine, "=");
        if (configKeyValuePair.size() >= 2) {
            (*pConfig)[configKeyValuePair[0]] = configKeyValuePair[1];
        }
    }
    configFile.close();
    return 0;
}
