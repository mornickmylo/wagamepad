#include "to-x-server-bool.h"
#include <X11/Xlib.h>

int toXServerBool(bool value)
{
    return value ? True : False;
}
