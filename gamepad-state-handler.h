#ifndef _GAMEPAD_STATE_HANDLER_H_
#define _GAMEPAD_STATE_HANDLER_H_

#include <string>
#include <set>
#include <vector>
#include <map>
#include "game-functions/range-of-keys-game-function.h"
#include "game-functions/two-keys-game-function.h"
#include "game-functions/single-key-game-function.h"
#include "game-functions/mouse-moving-function.h"
#include "game-functions/mouse-key-function.h"
#include <X11/Xlib.h>

enum MappingType {
    UnknownMappingName,
    GamepadBoolState,
    GamepadNumericState,
    GamepadNumericStateNumericSetting,
    GamepadNumericStateCurveSetting
};

class GamepadStateHandler {
private:
    std::map<MappingType, std::set<std::string>> functionNames;
    std::map<std::string, std::vector<std::string>> mappings;
    std::map<std::string, std::string> curveSettings;
    std::map<std::string, double> mouseSpeedSettings;
    std::map<std::string, std::unordered_set<KeySym>> singleKeyFunctionKeySyms;
    RangeOfKeysGameFunction* pWeaponsFunction;
    RangeOfKeysGameFunction* pTimerFunction;
    TwoKeysGameFunction* pCameraToggle;
    TwoKeysGameFunction* pGrenadeBounceToggle;
    std::vector<SingleKeyGameFunction> singleKeyFunctions;
    std::vector<MouseMovingFunction> mouseMovingFunctions;
    std::vector<MouseKeyGameFunction> mouseKeyFunctions;
    Display* _display;

    bool IsTriggerActive(std::set<std::string> gamepadState, std::set<std::string> trigger, bool fullEqualityOnly);
    MappingType getMappingNameType(std::string mappingKey);

    template<class T>
    std::set<T> toSet(std::vector<T> vec);
    std::string GetError(MappingType mappingType, std::string strWithMistake);
public:
    GamepadStateHandler(Display* display);
    std::string AddMapping(std::string functionKey, std::string mappingString);
    std::string FinishMapping();
    void OnGamepadStateChanged(std::set<std::string> gamepadBoolState, std::map<uint8_t, int16_t> gamepadNumericState);
};

#endif
