#ifndef MOUSE_MOVING_FUNCTION
#define MOUSE_MOVING_FUNCTION

#include <string>
#include <X11/Xlib.h>
#include <map>
#include <set>

enum MouseMovingDirection {
    Horizontal,
    Vertical
};

class MouseMovingFunction {
private:
    std::set<std::string> _trigger;
    uint8_t _dependency;
    Display* _display;
    MouseMovingDirection _mouseMovingDirection;
    double _maxSpeed;
    std::map<std::string, double (MouseMovingFunction::*)(double)> calsSpeedFunctions;
    std::string _curve;
    int inputMax;
    uint16_t XEventFireInterval;
    double speed;
    bool XEventFireTimerActive;

    double CalcSpeedLinear(double value);
    double CalcSpeedQuadratic(double value);
    double CalcSpeedCubic(double value);
    double CalcSpeedQuadraticExtreme(double value);
    double CalcSpeedSquareRoot(double value);
    uint8_t startXEventFireTimer();
    void stopXEventFireTimer();
public:
    MouseMovingFunction(Display* display, MouseMovingDirection mouseMovingDirection, double maxSpeed, std::set<std::string> trigger, uint8_t dependency, std::string curve);
    void SetState(double newState);
    const uint8_t GetDependency();
    const std::set<std::string> GetTrigger();
    void XEventFireTimerTick();
};
#endif
