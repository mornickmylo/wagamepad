#include "mouse-moving-function.h"
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#include <set>
#include <thread>
#include <iostream>
#include <cmath>
#include "../defaults.h"

MouseMovingFunction::MouseMovingFunction(Display* display, MouseMovingDirection mouseMovingDirection, double maxSpeed, std::set<std::string> trigger, uint8_t dependency, std::string curve)
{
    this->_display = display;
    this->_mouseMovingDirection = mouseMovingDirection;
    this->_maxSpeed = maxSpeed;
    this->_dependency = dependency;
    this->_trigger = trigger;
    this->_curve = curve;
    this->calsSpeedFunctions = {
        {"Linear", &MouseMovingFunction::CalcSpeedLinear},
        {"Quadratic", &MouseMovingFunction::CalcSpeedQuadratic},
        {"Cubic", &MouseMovingFunction::CalcSpeedCubic},
        {"QuadraticExtreme", &MouseMovingFunction::CalcSpeedQuadraticExtreme},
        {"SquareRoot", &MouseMovingFunction::CalcSpeedSquareRoot}
    };
    this->inputMax = MOUSE_MOVING_INPUT_MAX;
    this->XEventFireInterval = DEFAULT_MOUSE_MOVE_X_EVENT_SEND_INTERVAL_MS;
    this->XEventFireTimerActive = false;
}
void MouseMovingFunction::SetState(double newState)
{
    auto newSpeed = (this->*this->calsSpeedFunctions[this->_curve])(newState);
    if (newSpeed != 0 && this->speed == 0) {
        this->startXEventFireTimer();
    } else if (newSpeed == 0 && this->speed != 0) {
        this->stopXEventFireTimer();
    }
    this->speed = newSpeed;
}

double MouseMovingFunction::CalcSpeedLinear(double value)
{
    return value / (this->inputMax / this->_maxSpeed);
}
double MouseMovingFunction::CalcSpeedQuadratic(double value)
{
    double valueInRangeBetweenMinusOneAndOne = value / this->inputMax;
    return valueInRangeBetweenMinusOneAndOne * valueInRangeBetweenMinusOneAndOne * (value < 0 ? -1 : 1) * this->_maxSpeed;
}
double MouseMovingFunction::CalcSpeedCubic(double value)
{
    double valueInRangeBetweenMinusOneAndOne = value / this->inputMax;
    return valueInRangeBetweenMinusOneAndOne * valueInRangeBetweenMinusOneAndOne * valueInRangeBetweenMinusOneAndOne * this->_maxSpeed;
}
double MouseMovingFunction::CalcSpeedQuadraticExtreme(double value)
{
    double valueInRangeBetweenMinusOneAndOne = value / this->inputMax;
    return valueInRangeBetweenMinusOneAndOne
        * valueInRangeBetweenMinusOneAndOne
        * (valueInRangeBetweenMinusOneAndOne > 0.95 ? 1.5 : 1)
        * (value < 0 ? -1 : 1)
        * this->_maxSpeed;
}
double MouseMovingFunction::CalcSpeedSquareRoot(double value)
{
    return std::sqrt(std::abs(value / this->inputMax))
        * (value < 0 ? -1 : 1)
        * this->_maxSpeed;
}
const std::set<std::string> MouseMovingFunction::GetTrigger()
{
    return this->_trigger;
}
const uint8_t MouseMovingFunction::GetDependency()
{
    return this->_dependency;
}
void MouseMovingFunction::XEventFireTimerTick()
{
    XLockDisplay(this->_display);
    XTestFakeRelativeMotionEvent(this->_display,
        this->_mouseMovingDirection == MouseMovingDirection::Horizontal ? this->speed : 0,
        this->_mouseMovingDirection == MouseMovingDirection::Vertical ? this->speed : 0,
        0
    );
    XFlush(this->_display);
    XUnlockDisplay(this->_display);
}
uint8_t MouseMovingFunction::startXEventFireTimer()
{
    if (this->XEventFireTimerActive) {
        return 1;
    }
    this->XEventFireTimerActive = true;
    std::thread([&]() {
        while(1) {
            if (!this->XEventFireTimerActive) {
                break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(this->XEventFireInterval));
            this->XEventFireTimerTick();
        }
    }).detach();
    return 0;
}
void MouseMovingFunction::stopXEventFireTimer()
{
    this->XEventFireTimerActive = false;
}

