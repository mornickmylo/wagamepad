#include <X11/Xlib.h>
#include <string>
#include <set>
#include <iostream>
#include <unordered_set>
#include "two-keys-game-function.h"
#include <X11/extensions/XTest.h>
#include "../to-x-server-bool.h"

TwoKeysGameFunction::TwoKeysGameFunction(Display* display, std::unordered_set<KeySym> firstKeyWithModifiers, std::unordered_set<KeySym> secondKeyWithModifiers, std::set<std::string> trigger)
{
    for (const auto& key : firstKeyWithModifiers) {
        this->firstKeyWithModifiersCodes.insert(XKeysymToKeycode(display, key));
    }
    for (const auto& key : secondKeyWithModifiers) {
        this->secondKeyWithModifiersCodes.insert(XKeysymToKeycode(display, key));
    }
    this->state = false;
    this->_display = display;
    this->_trigger = trigger;
    this->isSecondKey = false;
}
void TwoKeysGameFunction::SetState(bool newState)
{
    if (newState != this->state) {
        if (newState) {
            this->isSecondKey = !this->isSecondKey;
        }
        XLockDisplay(this->_display);
        for (const auto& neededButton : this->GetNeededButtons()) {
            XTestFakeKeyEvent(this->_display, neededButton, toXServerBool(newState), 0);
        }
        XFlush(this->_display);
        XUnlockDisplay(this->_display);
        this->state = newState;
    }
}
std::unordered_set<unsigned int> TwoKeysGameFunction::GetNeededButtons()
{
    return this->isSecondKey ? this->secondKeyWithModifiersCodes : this->firstKeyWithModifiersCodes;
}
const std::set<std::string> TwoKeysGameFunction::GetTrigger()
{
    return this->_trigger;
}
