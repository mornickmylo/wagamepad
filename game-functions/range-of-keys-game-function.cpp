#include <X11/Xlib.h>
#include <string>
#include <set>
#include <vector>
#include <iostream>
#include "range-of-keys-game-function.h"
#include <X11/extensions/XTest.h>
#include "../to-x-server-bool.h"

RangeOfKeysGameFunction::RangeOfKeysGameFunction(Display* display, std::vector<std::vector<KeySym>> keys, std::set<std::string> triggerPrevious, std::set<std::string> triggerNext, std::set<std::string>* triggerSame)
{
    this->states = {{RangeOfKeysIndexAction::Decrease, false}, {RangeOfKeysIndexAction::Increase, false}, {RangeOfKeysIndexAction::Stay, false}};
    this->_display = display;
    this->_triggerPrevious = triggerPrevious;
    this->_triggerNext = triggerNext;
    this->index = 1;
    for (const auto& rangeElementKeys : keys) {
        std::vector<unsigned int> rangeElementKeyCodes;
        for (const auto& rangeElementKey : rangeElementKeys) {
            rangeElementKeyCodes.push_back(XKeysymToKeycode(display, rangeElementKey));
        }
        this->keyCodes.push_back(rangeElementKeyCodes);
    }
    if (triggerSame != nullptr) {
        this->_triggerSame = *triggerSame;
        this->haveTriggerSame = true;
    } else {
        this->haveTriggerSame = false;
    }
}
void RangeOfKeysGameFunction::SetState(bool newState, RangeOfKeysIndexAction indexAction)
{
    if (newState != this->states[indexAction]) {
        if (newState) {
            if (this->index > 0 && indexAction == RangeOfKeysIndexAction::Decrease) {
                this->index--;
            } else if (this->index < this->keyCodes.size() - 1 && indexAction == RangeOfKeysIndexAction::Increase) {
                this->index++;
            }
        }
        XLockDisplay(this->_display);
        for (const auto& neededButton : this->GetNeededButtons()) {
            XTestFakeKeyEvent(this->_display, neededButton, toXServerBool(newState), 0);
        }
        XFlush(this->_display);
        XUnlockDisplay(this->_display);
        this->states[indexAction] = newState;
    }
}
std::vector<unsigned int> RangeOfKeysGameFunction::GetNeededButtons()
{
    return this->keyCodes[this->index];
}
const std::set<std::string> RangeOfKeysGameFunction::GetTriggerNext()
{
    return this->_triggerNext;
}
const std::set<std::string> RangeOfKeysGameFunction::GetTriggerPrevious()
{
    return this->_triggerPrevious;
}
const std::set<std::string> RangeOfKeysGameFunction::GetTriggerSame()
{
    return this->_triggerSame;
}
bool RangeOfKeysGameFunction::HasTriggerSame()
{
    return this->haveTriggerSame;
}
