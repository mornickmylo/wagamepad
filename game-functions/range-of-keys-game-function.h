#ifndef RANGE_GAME_FUNCTION
#define RANGE_GAME_FUNCTION

#include <X11/Xlib.h>
#include <string>
#include <set>
#include <map>
#include <vector>

enum RangeOfKeysIndexAction {
    Stay,
    Increase,
    Decrease
};

class RangeOfKeysGameFunction {
private:
    std::vector<std::vector<unsigned int>> keyCodes;
    std::map<RangeOfKeysIndexAction, bool> states;
    Display* _display;
    std::set<std::string> _triggerPrevious;
    std::set<std::string> _triggerNext;
    std::set<std::string> _triggerSame;
    bool haveTriggerSame;
    uint8_t index;
    std::vector<unsigned int> GetNeededButtons();
public:
    RangeOfKeysGameFunction(Display* display, std::vector<std::vector<KeySym>> keys, std::set<std::string> triggerPrevious, std::set<std::string> triggerNext, std::set<std::string>* triggerSame);
    void SetState(bool newState, RangeOfKeysIndexAction indexAction);
    const std::set<std::string> GetTriggerPrevious();
    const std::set<std::string> GetTriggerNext();
    const std::set<std::string> GetTriggerSame();
    bool HasTriggerSame();
};
#endif
