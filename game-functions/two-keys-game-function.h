#ifndef SWITCH_GAME_FUNCTION
#define SWITCH_GAME_FUNCTION

#include <X11/Xlib.h>
#include <string>
#include <set>
#include <unordered_set>

class TwoKeysGameFunction {
private:
    std::unordered_set<unsigned int> firstKeyWithModifiersCodes;
    std::unordered_set<unsigned int> secondKeyWithModifiersCodes;
    bool state;
    Display* _display;
    std::set<std::string> _trigger;
    bool isSecondKey;
    std::unordered_set<unsigned int> GetNeededButtons();
public:
    TwoKeysGameFunction(Display* display, std::unordered_set<KeySym> firstKeyWithModifiers, std::unordered_set<KeySym> secondKeyWithModifiers,  std::set<std::string> trigger);
    void SetState(bool newState);
    const std::set<std::string> GetTrigger();
};
#endif
