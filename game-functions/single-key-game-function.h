#ifndef PRESSING_GAME_FUNCTION
#define PRESSING_GAME_FUNCTION

#include <X11/Xlib.h>
#include <string>
#include <set>
#include <unordered_set>

class SingleKeyGameFunction {
private:
    std::unordered_set<unsigned int> keyAndModifiersCodes;
    bool state;
    Display* _display;
    std::set<std::string> _trigger;
public:
    SingleKeyGameFunction(Display* display, std::unordered_set<KeySym> keyWithModifiers, std::set<std::string> trigger);
    void SetState(bool newState);
    const std::set<std::string> GetTrigger();
};
#endif
