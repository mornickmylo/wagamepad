#include "single-key-game-function.h"
#include "../to-x-server-bool.h"
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>

SingleKeyGameFunction::SingleKeyGameFunction(Display* display, std::unordered_set<KeySym> keyWithModifiers, std::set<std::string> trigger)
{
    for (const auto& key : keyWithModifiers) {
        this->keyAndModifiersCodes.insert(XKeysymToKeycode(display, key));
    }
    this->state = false;
    this->_display = display;
    this->_trigger = trigger;
}

const std::set<std::string> SingleKeyGameFunction::GetTrigger()
{
    return this->_trigger;
}
void SingleKeyGameFunction::SetState(bool newState)
{
    if (this->state != newState)
    {
        this->state = newState;
        XLockDisplay(this->_display);
        for (const auto& pressedKeyboardKey : this->keyAndModifiersCodes) {
            XTestFakeKeyEvent(this->_display, pressedKeyboardKey, toXServerBool(newState), 0);
        }
        XFlush(this->_display);
        XUnlockDisplay(this->_display);
    }
}

