
#ifndef MOUSE_KEY_FUNCTION
#define MOUSE_KEY_FUNCTION

#include <X11/Xlib.h>
#include <string>
#include <set>

class MouseKeyGameFunction {
private:
    unsigned int _mouseKey;
    bool state;
    Display* _display;
    std::set<std::string> _trigger;
public:
    MouseKeyGameFunction(Display* display, unsigned int mouseKey, std::set<std::string> trigger);
    void SetState(bool newState);
    const std::set<std::string> GetTrigger();
};
#endif
