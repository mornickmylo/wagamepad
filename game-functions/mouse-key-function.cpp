#include "mouse-key-function.h"
#include "../to-x-server-bool.h"
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#include <iostream>

MouseKeyGameFunction::MouseKeyGameFunction(Display* display, unsigned int mouseKey, std::set<std::string> trigger)
{
    this->_mouseKey = mouseKey;
    this->state = false;
    this->_display = display;
    this->_trigger = trigger;
}

const std::set<std::string> MouseKeyGameFunction::GetTrigger()
{
    return this->_trigger;
}
void MouseKeyGameFunction::SetState(bool newState)
{
    if (this->state != newState)
    {
        this->state = newState;
        XLockDisplay(this->_display);
        XTestFakeButtonEvent(this->_display, this->_mouseKey, newState, 0);
        XFlush(this->_display);
        XUnlockDisplay(this->_display);
    }
}
