#ifndef _WA_GAMEPAD_H_
#define _WA_GAMEPAD_H_

#include <SDL.h>
#include <set>
#include <map>
#include <X11/Xlib.h>
#include "gamepad-state-handler.h"

class WAGamepad {
public:
    WAGamepad();
    std::string start(int joystickIndex, std::string configPath);
private:
    SDL_Event e;
    SDL_Joystick* gGameController;
    std::set<Uint32> gamepadEvents;
    Display* display;
    GamepadStateHandler* handler;
    int joystickBoolStateDeadZone;
    int joystickNumericStateDeadZone;
    std::set<int>* triggers;
    std::set<std::string> gamepadBoolState;
    std::map<uint8_t, int16_t> gamepadNumericState;

    uint8_t ReadConfig(std::string configPath, std::map<std::string, std::string>* pConfig);
    void mainLoop();
    void getTriggers();
    void handleGemapadEvent();
    void logStringSetIfChanged(std::set<std::string> set);
};

#endif
